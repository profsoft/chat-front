import React, { Component } from 'react';

class Message extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isMessageOfCurrentUser: this.props.isMessageOfCurrentUser || false,
      postedTime: this.props.postedTime || new Date()
    };
  }

  addZero = (i) => {
    if (i < 10) {
      i = "0" + i;
    }
    return i;
  }

  render_time = (time) => {
    var h = this.addZero(time.getHours());
    var m = this.addZero(time.getMinutes());
    return h + ":" + m;
  }

  render() {
    var sectionClass = this.state.isMessageOfCurrentUser ? "message right" : "message left";
    var user = this.state.isMessageOfCurrentUser ? "Я" : this.props.userId;

    return (
      <section className={sectionClass} >
        <div className="message-user">{user}</div>
        <div className="message-text">
          {this.props.message}
        </div>
        <div className="message-datetime">{this.render_time(this.state.postedTime)}</div>
      </section>
    );
  }
}

export default Message;
