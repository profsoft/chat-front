import React, { Component } from 'react';
import { getUserConnectedIdLocation } from '../api';
import { Link } from 'react-router-dom';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

class Intro extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount = function () {
    getUserConnectedIdLocation((newUserId, suggestion) => {
      this.setState({
        isAddressSent: true,
        userId: newUserId,
        address: suggestion.value
      });
    });
  }

  state = {
    address: '',
    isAddressSent: false,
    address: ''
  }

  handleAddressInputChange = (event) => {
    this.setState({ address: event.target.value });
  }

  render_link = () => {
    var linkBlock;
    const pathParams = {
      pathname: '/main',
      currentUserId: this.state.userId
    };

    if (this.state.isAddressSent === true) {
      linkBlock = <Link to={pathParams}>Начать</Link>
    }
    else {
      linkBlock = <span>Начать</span>
    }

    return linkBlock;
  }

  render() {
    return (
      <div className="intro-screen">
        <section className="address-section">
          <h5>
            Для продолжения введите адрес
          </h5>
          <Form>
            <Form.Group id="formBasicEmail">
              <Form.Control id="address" name="address" type="text" value={this.state.address} onChange={this.handleAddressInputChange} placeholder="Введите адрес" />
            </Form.Group>

            <Button variant="light">{this.render_link()}</Button>
          </Form>
        </section>
      </div>

    );
  }
}

export default Intro;
