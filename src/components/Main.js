import React, { Component } from 'react';
import { subscribeToSendRoutine } from '../api';
import Message from './Message';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

class Main extends Component {
  constructor(props) {
    super(props);

    this.handleMessageAreaChange = this.handleMessageAreaChange.bind(this);
    this.handleSendMessageButtonClick = this.handleSendMessageButtonClick.bind(this);

    this.state = {
      messages: [],
      // {
      //   userId: 'Guest 1',
      //   message: 'Mr. Black is on the line',
      //   datetime: new Date(2018, 11, 12, 8, 12, 33),
      // },
      // {
      //   userId: 'Guest 1',
      //   message: 'Ok, what\'s next? ..Mr. Black is on the line',
      //   datetime: new Date(2018, 11, 13, 21, 10, 3),
      // }
      // ],
      currentUserId: this.props.location !== undefined ? this.props.location.currentUserId || 'unknown' : ''
    };
  }

  state = {
    messages: [],
    message: ''
  };

  handleMessageAreaChange(event) {
    this.setState({ message: event.target.value });
  }

  handleSendMessageButtonClick(event) {
    subscribeToSendRoutine((err, userId, messageText) => {
      var message = {
        userId: userId,
        message: messageText,
        datetime: new Date()
      };

      var currentMessages = this.state.messages;
      console.log(currentMessages.some(e => e.message === message.message));

      // var recentPart = currentMessages.splice(currentMessages.length - 10, currentMessages.length - 1);
      // console.log('argh', recentPart);

      if (!currentMessages.some(e => e.message === message.message && e.userId === message.userId)) {
        currentMessages.push(message);
        this.setState({ messages: currentMessages });
        console.log('work!', currentMessages);
      }
    }, this.state.message);

    this.setState({ message: '' });
  }

  handleKeyPress = () => {
    var key = window.event.keyCode;
    if (key === 13) {
      this.handleSendMessageButtonClick(window.event);
    }
  };

  renderMessageInfo = () => {
    console.log('123!!!');
    const { messages } = this.state
    let messageBlocks = []

    if (messages.length) {
      console.log('About to render messages', messages)
      messageBlocks = messages.map((message) => {
        return (
          <Message
            key={messages.indexOf(message)}
            userId={message.userId}
            message={message.message}
            postedTime={message.datetime}
            isMessageOfCurrentUser={this.state.currentUserId === message.userId} />
        )
      })
    }

    return messageBlocks;
  }

  render() {
    return (
      <div className="message-screen">
        <section className="messages">
          {this.renderMessageInfo()}
        </section>
        <div className="message-send-controls">
          <Form.Control as="textarea"
            id="message"
            value={this.state.message}
            onChange={this.handleMessageAreaChange}
            onKeyPress={this.handleKeyPress} placeholder="Write message" />

          <Button variant="light" size="sm" onClick={this.handleSendMessageButtonClick}>
            <img src={require('../assets/send-icon.jpg')} alt='…' />
          </Button>
        </div>
      </div >
    );
  }
}

export default Main;
