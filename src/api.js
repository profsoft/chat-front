import openSocket from 'socket.io-client'
import $ from "jquery";
import { suggestions } from "suggestions-jquery";

const socket = openSocket('http://localhost:3000');

function subscribeToSendRoutine(cb, messageText) {
    socket.emit('send mess', { text: messageText });
    socket.on('add mess', data => {
        cb(null, data.userId, data.text);
    });
}

function getUserConnectedIdLocation(storageFunc) {
    socket.on("new_username", function (data) {
        var userId = data.userId;
        $("#address").suggestions({
            token: "ca625b37d4ee631282e07b63ab992e93af81a005",
            type: "ADDRESS",
            onSelect: function (suggestion) {
                socket.emit('update_user_gps', { userId: data.userId, ltd: suggestion['data']['geo_lat'], lng: suggestion['data']['geo_lon'] });
                storageFunc(userId, suggestion);
            }
        });
    });
}

function getUserDisconnectedId(storageFunc) {
    socket.on("new_disconnected", function (data) {
        var userId = data.userId;
        storageFunc(userId);
    });

}

export { subscribeToSendRoutine, getUserConnectedIdLocation }