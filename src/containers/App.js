import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import Intro from '../components/Intro';
import Main from '../components/Main';
// require('../assets//background.jpg')

class App extends Component {
  render() {
    return (
      <div className="ChatApp" >
        <Switch>
          <Route exact path="/" component={Intro} />
          <Route exact path="/main" component={Main} />
        </Switch>
      </div>
    );
  }
}

export default App;
